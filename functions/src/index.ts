import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

export const dibs = functions.https.onCall(
    async (args: { emailadres: string; productId: string } | null, context) => {
        if (args === null) return true;
        const { emailadres, productId } = args;
        const [productsnapshot, dibsensnapshot] = await Promise.all([
            admin
                .firestore()
                .collection("products")
                .doc(productId)
                .get(),
            admin
                .firestore()
                .collection("dibsen")
                .where("productId", "==", productId)
                .get()
        ]);
        const product = productsnapshot.data() as Product;
        const dibsen = dibsensnapshot.docs.map(
            snapshot => snapshot.data() as { emailadres: string; productId: string }
        );

        if (
            dibsen.length < product.aantal &&
            !dibsen.find(d => d.emailadres === emailadres)
        ) {
            await admin
                .firestore()
                .collection("dibsen")
                .doc(`${emailadres}:${productId}`)
                .set({ emailadres, productId });
            return true;
        } else {
            return false;
        }
    }
);

interface Product {
    naam: string;
    per: number;
    aantal: number;
    prijs: number;
    img: string;
}
