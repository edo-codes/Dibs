"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
exports.dibs = functions.https.onCall((args, context) => __awaiter(this, void 0, void 0, function* () {
    if (args === null)
        return true;
    const { emailadres, productId } = args;
    const [productsnapshot, dibsensnapshot] = yield Promise.all([
        admin
            .firestore()
            .collection("products")
            .doc(productId)
            .get(),
        admin
            .firestore()
            .collection("dibsen")
            .where("productId", "==", productId)
            .get()
    ]);
    const product = productsnapshot.data();
    const dibsen = dibsensnapshot.docs.map(snapshot => snapshot.data());
    if (dibsen.length < product.aantal &&
        !dibsen.find(d => d.emailadres === emailadres)) {
        yield admin
            .firestore()
            .collection("dibsen")
            .doc(`${emailadres}:${productId}`)
            .set({ emailadres, productId });
        return true;
    }
    else {
        return false;
    }
}));
//# sourceMappingURL=index.js.map