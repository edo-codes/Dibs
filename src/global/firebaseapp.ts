import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";

const app =
    firebase.apps[0] ||
    (() => {
        console.log("initializing app");
        return firebase.initializeApp({
            apiKey: "AIzaSyAdG0dUdBiUbYUzEFfh980cDFds4PCNu_w",
            authDomain: "edo-dibs.firebaseapp.com",
            databaseURL: "https://edo-dibs.firebaseio.com",
            projectId: "edo-dibs",
            storageBucket: "edo-dibs.appspot.com",
            messagingSenderId: "331991992427"
        });
    })();

app.firestore().settings({
    timestampsInSnapshots: true
});
firebase.firestore.setLogLevel("error");

export default app;
