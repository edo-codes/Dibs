import "@firebase/firestore";

export interface Product {
    naam: string;
    per: number;
    aantal: number;
    prijs: number;
    img: string;
    added: firebase.firestore.Timestamp;
    publiceren: boolean;
}

export interface Dibs {
    emailadres: string;
    productId: string;
}

export function productEquals(a: Product, b: Product): boolean {
    for (const p of Object.getOwnPropertyNames(a).concat(Object.getOwnPropertyNames(b))) {
        if (!["aantal", "added", "img", "naam", "per", "prijs", "publiceren"].includes(p)) {
            throw new Error(`productEquals(): Unknown property: ${p}`);
        }
    }
    const equal =
        a.aantal === b.aantal &&
        a.added.isEqual(b.added) &&
        a.img === b.img &&
        a.naam === b.naam &&
        a.per === b.per &&
        a.prijs === b.prijs &&
        a.publiceren === b.publiceren;
    if (!equal) console.log(a, "!==", b);
    return equal;
}

(window as any).productEquals = productEquals;
