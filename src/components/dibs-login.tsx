import firebase from "@firebase/app";
import "@firebase/auth";
import { Component, State } from "@stencil/core";
import { authState } from "rxfire/auth";
import firebaseapp from "../global/firebaseapp";

@Component({
    tag: "dibs-login"
})
export class DibsLogin {
    @State() public loggedin: boolean | "Loading" = "Loading";

    public componentWillLoad() {
        authState(firebaseapp.auth()).subscribe(u => {
            this.loggedin = !!u && !!u.email;
            firebaseapp
                .functions()
                .httpsCallable("dibs")(null)
                .then(({ data }) => {
                    console.log("warmed up functions, result: ", data);
                });
        });
    }

    public signOut() {
        firebaseapp.auth().signOut();
    }

    public async signIn() {
        this.loggedin = "Loading";
        const cred = await firebaseapp
            .auth()
            .signInWithPopup(new firebase.auth!.GoogleAuthProvider());
    }

    public render() {
        if (this.loggedin === "Loading") {
            return <ion-spinner />;
        } else if (this.loggedin) {
            return (
                <ion-button onClick={() => this.signOut()} fill="clear">
                    Uitloggen
                </ion-button>
            );
        } else {
            return (
                <ion-button onClick={() => this.signIn()} color="primary">
                    Inloggen
                </ion-button>
            );
        }
    }
}
