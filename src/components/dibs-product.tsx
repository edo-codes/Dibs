import { Component, Prop, State } from "@stencil/core";
import { collectionData } from "rxfire/firestore";
import firebaseapp from "../global/firebaseapp";
import { Dibs, Product } from "../global/product";

@Component({
    tag: "dibs-product",
    styleUrl: "dibs-product.css"
})
export class DibsProduct {
    @Prop() public product: Product;
    @Prop() public productid: string;

    @State() public over: number | "Loading" = "Loading";
    @State() public gedibst: boolean | "Loading" = "Loading";

    public componentWillLoad() {
        collectionData<Dibs>(
            firebaseapp
                .firestore()
                .collection("dibsen")
                .where("productId", "==", this.productid)
        ).subscribe(s => {
            this.over = this.product.aantal - s.length;
            this.gedibst = !!s.find(
                x =>
                    x.emailadres.toLowerCase() ===
                    firebaseapp.auth().currentUser!.email!.toLowerCase()
            );
        });
    }

    get emailadres() {
        return !!firebaseapp.auth().currentUser ? firebaseapp.auth().currentUser!.email : null;
    }

    public async dibs() {
        this.gedibst = "Loading";
        firebaseapp
            .functions()
            .httpsCallable("dibs")({
                productId: this.productid,
                emailadres: this.emailadres
            })
            .then(success => {
                if (!success as boolean) {
                    console.error("Dibs failed");
                }
            });
    }
    public async ontDibs() {
        await firebaseapp
            .firestore()
            .collection("dibsen")
            .doc(`${this.emailadres}:${this.productid}`)
            .delete();
    }

    public render() {
        const { naam, per, aantal, prijs, img } = this.product;
        return (
            <ion-card style={{ margin: "0" }}>
                <img
                    src={img}
                    style={{
                        objectFit: "cover",
                        height: "190px",
                        backgroundColor: "lightgrey"
                    }}
                />
                <ion-card-header>
                    <ion-card-subtitle style={{ float: "right", "font-size": "120%" }}>
                        <span style={{ opacity: "0.6", "font-size": "110%" }}>€ </span>
                        {prijs.toFixed(2).toString()}
                    </ion-card-subtitle>
                    <ion-card-title style={{ "margin-top": "0" }}>{naam || "???"}</ion-card-title>
                    <ion-card-subtitle>{per} per verpakking</ion-card-subtitle>
                </ion-card-header>
                <ion-card-content>
                    <p>
                        Nog {this.over === "Loading" ? "..." : <strong>{this.over}</strong>} van de{" "}
                        {aantal} over
                    </p>
                </ion-card-content>
                {this.gedibst === "Loading" ? (
                    <ion-button class="bottombutton" expand="full" color="light" disabled>
                        <ion-spinner color="dark" />
                    </ion-button>
                ) : this.gedibst === false ? (
                    <ion-button
                        class="bottombutton"
                        expand="full"
                        color="success"
                        onClick={() => this.dibs()}
                    >
                        Dibs
                    </ion-button>
                ) : (
                    <ion-button
                        class="bottombutton"
                        expand="full"
                        color="danger"
                        onClick={() => this.ontDibs()}
                    >
                        Never mind
                    </ion-button>
                )}
            </ion-card>
        );
    }
}
