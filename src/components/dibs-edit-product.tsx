import { Component, Prop, State } from "@stencil/core";
import firebaseapp from "../global/firebaseapp";
import { Product, productEquals } from "../global/product";

@Component({
    tag: "dibs-edit-product",
    styleUrl: "dibs-edit-product.css"
})
export class DibsEditProduct {
    @Prop() public product: Product;
    @Prop() public productid: string;

    @State() private imageURL: string;
    @State() private editedProduct: Product;

    public componentWillLoad() {
        this.imageURL = this.product.img;
        this.editedProduct = { ...this.product };
    }

    async deleteProduct() {
        await firebaseapp
            .firestore()
            .collection("products")
            .doc(this.productid)
            .delete();
    }

    async saveProduct() {
        await firebaseapp
            .firestore()
            .collection("products")
            .doc(this.productid)
            .set(this.editedProduct);
    }

    private setImage(file: File) {
        this.imageURL = URL.createObjectURL(file);
        this.editedProduct = { ...this.editedProduct, img: this.imageURL };
    }

    public render() {
        const { naam, per, aantal, prijs, img, added, publiceren } = this.editedProduct;
        return (
            <ion-card>
                <div id="fotocontainer">
                    <label id="fotopickerlabel" htmlFor={"fotopicker" + this.productid}>
                        <img src={this.imageURL} style={{ objectFit: "cover", height: "190px" }} />
                        <div id="fotooverlay" />
                        <div id="fototext">Nieuwe foto kiezen...</div>
                    </label>
                    <input
                        id={"fotopicker" + this.productid}
                        class="fotopicker"
                        type="file"
                        accept="image/*"
                        capture="environment"
                        onInput={ev => this.setImage((ev.target as HTMLInputElement).files![0]!)}
                    />
                </div>

                <ion-card-content>
                    <ion-grid id="formgrid">
                        <ion-row>
                            <ion-col>
                                <ion-item>
                                    <ion-label position="floating">Naam</ion-label>
                                    <ion-input
                                        id="input-naam"
                                        type="text"
                                        value={naam}
                                        onIonChange={e => {
                                            this.editedProduct = {
                                                ...this.editedProduct,
                                                naam: (e.target as HTMLIonInputElement).value!
                                            };
                                        }}
                                    />
                                </ion-item>
                            </ion-col>
                        </ion-row>
                        <ion-row>
                            <ion-col>
                                <ion-item>
                                    <ion-label position="floating">Per verpakking</ion-label>
                                    <ion-input
                                        type="number"
                                        step="1"
                                        value={per.toString()}
                                        onIonChange={e => {
                                            this.editedProduct = {
                                                ...this.editedProduct,
                                                per: parseInt(
                                                    (e.target as HTMLIonInputElement).value!,
                                                    10
                                                )
                                            };
                                        }}
                                    />
                                </ion-item>
                            </ion-col>
                            <ion-col>
                                <ion-item>
                                    <ion-label position="floating">Aantal</ion-label>
                                    <ion-input
                                        type="number"
                                        step="1"
                                        value={aantal.toString()}
                                        onIonChange={e => {
                                            this.editedProduct = {
                                                ...this.editedProduct,
                                                aantal: parseInt(
                                                    (e.target as HTMLIonInputElement).value!,
                                                    10
                                                )
                                            };
                                        }}
                                    />
                                </ion-item>
                            </ion-col>
                            <ion-col>
                                <ion-item>
                                    <ion-label position="floating">Prijs €</ion-label>
                                    <ion-input
                                        type="number"
                                        step="0.05"
                                        value={prijs.toFixed(2)}
                                        onIonChange={e => {
                                            this.editedProduct = {
                                                ...this.editedProduct,
                                                prijs: parseFloat(
                                                    (e.target as HTMLIonInputElement).value!
                                                )
                                            };
                                        }}
                                    />
                                </ion-item>
                            </ion-col>
                        </ion-row>
                    </ion-grid>
                </ion-card-content>
                <ion-grid>
                    <ion-row>
                        <ion-col size="auto">
                            <ion-item>
                                <ion-label>Publiceren:</ion-label>
                                <ion-toggle
                                    slot="end"
                                    checked={publiceren}
                                    onIonChange={e => {
                                        this.editedProduct = {
                                            ...this.editedProduct,
                                            publiceren: (e.target as HTMLIonToggleElement).checked
                                        };
                                    }}
                                />
                            </ion-item>
                        </ion-col>
                        <ion-col />
                        <ion-col size="auto">
                            <ion-button color="danger" onClick={() => this.deleteProduct()}>
                                <ion-icon name="trash" />
                            </ion-button>
                            <ion-button
                                onClick={() => this.saveProduct()}
                                color="success"
                                disabled={productEquals(this.product, this.editedProduct)}
                            >
                                Opslaan
                            </ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>
            </ion-card>
        );
    }
}
