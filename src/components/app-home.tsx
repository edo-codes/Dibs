import { Component, State } from "@stencil/core";
import { authState } from "rxfire/auth";
import { collection } from "rxfire/firestore";
import { map } from "rxjs/operators";
import firebaseapp from "../global/firebaseapp";
import { Product } from "../global/product";

@Component({
    tag: "app-home",
    styleUrl: "app-home.css"
})
export class AppHome {
    @State() public products: Array<[string, Product]> | "Loading" = "Loading";
    @State() public loggedIn: boolean | "Loading" = "Loading";

    public componentWillLoad() {
        collection(
            firebaseapp
                .firestore()
                .collection("products")
                .orderBy("added")
        )
            .pipe(map(docs => docs.map<[string, Product]>(doc => [doc.id, doc.data() as Product])))
            .subscribe(docs => {
                this.products = docs;
            });
        authState(firebaseapp.auth()).subscribe(u => {
            this.loggedIn = !!u && !!u.email;
        });
    }

    public render() {
        if (this.loggedIn !== true) {
            return (
                <ion-content>
                    <dibs-login />
                </ion-content>
            );
        } else if (this.products === "Loading") {
            return <ion-spinner />;
        } else {
            return [
                <ion-toolbar color="primary">
                    <ion-buttons slot="start">
                        <ion-title>Dibs</ion-title>
                    </ion-buttons>
                    <ion-buttons slot="end">
                        <ion-button fill="clear" href="/edit">
                            Producten beheren
                        </ion-button>
                        <dibs-login />
                    </ion-buttons>
                </ion-toolbar>,
                <ion-content>
                    <div class="productgrid">
                        {this.products.map(([id, product]) => (
                            <dibs-product class="product" productid={id} product={product} />
                        ))}
                    </div>
                </ion-content>
            ];
        }
    }
}
