import firebase from "@firebase/app";
import "@firebase/firestore";
import { Component, State } from "@stencil/core";
import { authState } from "rxfire/auth";
import { collection } from "rxfire/firestore";
import { map } from "rxjs/operators";
import firebaseapp from "../global/firebaseapp";
import { Product } from "../global/product";

@Component({
    tag: "app-edit",
    styleUrl: "app-edit.css"
})
export class AppHome {
    @State() public products: Array<[string, Product]> | "Loading" = "Loading";
    @State() public loggedIn: boolean | "Loading" = "Loading";

    public async componentWillLoad() {
        collection(
            firebaseapp
                .firestore()
                .collection("products")
                .orderBy("added")
        )
            .pipe(map(docs => docs.map<[string, Product]>(doc => [doc.id, doc.data() as Product])))
            .subscribe(docs => {
                this.products = docs;
            });
        authState(firebaseapp.auth()).subscribe(u => {
            this.loggedIn = !!u && !!u.email;
        });
    }

    newproduct() {
        firebaseapp
            .firestore()
            .collection("products")
            .add({
                aantal: 1,
                img: "",
                naam: "",
                per: 1,
                prijs: 0,
                added: firebase.firestore!.Timestamp.fromDate(new Date())
            } as Product);
    }

    public render() {
        if (this.loggedIn === false) {
            return <stencil-route-redirect url="/" />;
        } else if (this.loggedIn === "Loading") {
            return <ion-loader />;
        } else if (this.loggedIn === true) {
            return [
                <ion-toolbar color="primary">
                    <ion-buttons slot="start">
                        <ion-back-button defaultHref="/" />
                    </ion-buttons>
                    <ion-title>Producten beheren</ion-title>
                </ion-toolbar>,
                <ion-content>
                    <div class="productgrid">
                        {this.products === "Loading" ? (
                            <ion-spinner />
                        ) : (
                            [
                                this.products.map(([id, product]) => (
                                    <dibs-edit-product
                                        class="product"
                                        productid={id}
                                        product={product}
                                    />
                                )),
                                <ion-card class="product">
                                    <ion-button onClick={() => this.newproduct()} expand="block">
                                        <ion-icon name="add" />
                                    </ion-button>
                                </ion-card>
                            ]
                        )}
                    </div>
                </ion-content>
            ];
        }
    }
}
